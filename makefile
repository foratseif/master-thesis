all: pdf

pdf: main.tex mybib.bib
	pdflatex main.tex
	biber    main
	pdflatex main.tex

milestone: pdf
	cp main.pdf Thesis.pdf

clean:
	rm *.aux *.bbl *.bcf *.blg *.lof *.log *.lot *.run.xml *.toc

